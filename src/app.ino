/* Copyright 2018 Vyacheslav Kononenko */

/* This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#include <Arduino.h>
#include <TimerOne.h>

const byte numLeds = 50;
const byte progLeds = 30;

byte leds[ 4 ][ numLeds ];

byte convertColorHi( byte cl )
{
    byte res = 0x80;
    switch( cl & ( 3 << 4 ) ) {
    case 1 << 4 : res |= 7 << 2;
        break;
    case 2 << 4 : res |= 15 << 2;
        break;
    case 3 << 4 : res |= 31 << 2;
        break;
    }
    switch( cl & ( 3 << 2 ) ) {
    case 2 << 2 : res |= 1;
        break;
    case 3 << 2 : res |= 3;
        break;
    }
    return res;
}

byte convertColorLo( byte cl )
{
    byte res = cl & ( 3 << 2 ) ? 7 << 5 : 0;
    switch( cl & 3 ) {
    case 1 : res |= 7;
        break;
    case 2 : res |= 15;
        break;
    case 3 : res |= 31;
        break;
    }
    return res;
}

enum Mode {
    mStart,
    mHeader,
    mConvertHi,
    mConvertLo,
    mData,
    mBlank
};

bool refresh = false;

ISR( TIMER1_COMPA_vect )
{
    static byte currentLed = 0;
    static byte colors[4];
    static byte counter = 0;
    static Mode m = mStart;

    byte mask = 0;

    switch( m ) {
    case mStart :
        if( !refresh ) break;
        m = mHeader;
        counter = 0;
        // no break;
    case mHeader :
        switch( ++counter ) {
        case 1  : mask = 15 << 1; break;
        case 33 : m = mConvertHi; break;
        }
        break;
    case mConvertHi :
        counter = 0;
        // no break
    case mConvertLo :
        for( byte i = 0; i < 4; ++i )
            colors[i] = m == mConvertHi ? convertColorHi( leds[i][ currentLed ] ) :
                                          convertColorLo( leds[i][ currentLed ] );
        if( m == mConvertLo )
            ++currentLed;
        m = mData;
        // no break
    case mData :
        asm( "ld  __tmp_reg__,%a2 \n\t"
             "lsl __tmp_reg__ \n\t"
             "st %a2+,__tmp_reg__ \n\t"
             "rol %0 \n\t"
             "ld  __tmp_reg__,%a2 \n\t"
             "lsl __tmp_reg__ \n\t"
             "st %a2+,__tmp_reg__ \n\t"
             "rol %0 \n\t"
             "ld  __tmp_reg__,%a2 \n\t"
             "lsl __tmp_reg__ \n\t"
             "st %a2+,__tmp_reg__ \n\t"
             "rol %0 \n\t"
             "ld  __tmp_reg__,%a2 \n\t"
             "lsl __tmp_reg__ \n\t"
             "st %a2+,__tmp_reg__ \n\t"
             "rol %0 \n\t"
             "lsl %0 \n\t"
        : "=r" (mask) : "0" (mask), "e" (colors)  );

        switch( ++counter ) {
        case 8 : m = mConvertLo; break;
        case 16 :
            if( currentLed == numLeds ) {
                m = mBlank;
                currentLed = 0;
                counter = 0;
            }
            else m = mConvertHi;
            break;
        }
        break;
    case mBlank :
        if( ++counter == 0 ) {
            m = mStart;
            refresh = false;
            //digitalWrite( 7, 0 );
        }
        break;
    }
    PORTB = mask;
    mask |= 0x20;
    PORTB = mask;
}

void setup()
{
    pinMode( 7, 1 );
    DDRB = 0x3E;
    memset( leds, 0, sizeof( leds ) );

    noInterrupts();
    TCCR1A = 0;
    TCCR1B = 0;
    TCNT1  = 0;

    OCR1A = 32;            // compare match register 8khz
    TCCR1B |= (1 << WGM12);   // CTC mode
    TCCR1B |= (1 << CS10);    // 256 prescaler
    TIMSK1 |= (1 << OCIE1A);  // enable timer compare interrupt
    interrupts();             // enable all interrupts
}

const byte midBlue   = 2 << 4;
const byte midRed    = 2 << 2;
const byte midGreen  = 2;
const byte brightBlue   = 3 << 4;
const byte brightRed    = 3 << 2;
const byte brightGreen  = 3;
const byte brightViolet = midBlue | brightRed;
const byte brightYellow = midGreen | brightRed;
const byte brightWhite = brightBlue | brightGreen | brightRed;

static byte i = 0, j = 0;
static byte color = 0;

class Effect {
public:
    virtual void init() = 0;
    virtual bool run() = 0;
};

class SetColor : public Effect {
public:
    SetColor( byte cl ) : m_color( cl ) {}
    virtual void init() { color = m_color; }
    virtual bool run() { return false; }
private:
    byte m_color;
};


class GroupEffect : public Effect {
public:
    typedef Effect * EffectPtr;
    template< class ...Args >
    GroupEffect( Args... args )
    {
        m_size = sizeof...( args );
        m_arr  = new EffectPtr[ m_size ];
        EffectPtr tmp[] = {args...};
        i = 0;
        for( EffectPtr p : tmp )
            m_arr[i++] = p;
    }

    virtual void init()
    {
        m_curr = 0;
        m_arr[0]->init();
    }

    virtual bool run()
    {
        if( m_arr[m_curr]->run() ) return true;
        if( ++m_curr >= m_size ) return false;
        m_arr[m_curr]->init();
        return true;
    }

private:
    Effect **m_arr;
    int      m_size;
    int      m_curr;
};

class AllFromBack : public Effect {
public:
    virtual void init()
    {
        i = 0;
    }

    virtual bool run()
    {
        for( byte ii = 0; ii < 4; ++ii )
            leds[ii][ progLeds - i - 1 ] = color;
        return ++i < progLeds;
    }
};

class Tracer : public Effect {
public:
    virtual void init()
    {
        i = 0;
        j = 0;
    }

    virtual bool run()
    {
        if( ++j < 2 ) return true;
        j = 0;
        for( byte ii = 0; ii < 4; ++ii )
            leds[ii][ progLeds - i - 1 ] = color;
        ++i;
        return ++i < progLeds;
    }
};

class Line : public Effect {
public:
    Line( byte line, bool up ) : m_line( line ), m_up( up ) {}

    virtual void init()
    {
        i = 0;
    }

    virtual bool run()
    {
        leds[m_line][ m_up ? progLeds - i - 1 : i] = color;
        return ++i < progLeds;
    }
private:
    byte m_line;
    bool m_up;
};

class Runner : public Effect {
public:
    Runner( byte line, bool up ) : m_line( line ), m_up( up ) {}

    virtual void init()
    {
        i = 0;
    }

    virtual bool run()
    {
        if( i > 0 )
            leds[m_line][ m_up ? progLeds - i : i - 1] = 0;
        if( i < progLeds)
            leds[m_line][ m_up ? progLeds - i - 1 : i] = color;
        return ++i <= progLeds;
    }
private:
    byte m_line;
    bool m_up;
};


class Set : public Effect {
public:
    Set( byte line ) : m_line( line ) {}

    virtual void init()
    {
        i = 0;
    }

    virtual bool run()
    {
        memset( leds[m_line], color, progLeds );
        return ++i < 8;
    }
private:
    byte m_line;
};

class Reset : public Effect {
public:
    Reset( byte line ) : m_line( line ) {}

    virtual void init()
    {
    }

    virtual bool run()
    {
        memset( leds[m_line], 0, progLeds );
        return false;
    }
private:
    byte m_line;
};


void loop()
{
    static Tracer tracer;
    static AllFromBack allFromBack;
    static SetColor colorOff( 0 );
    static SetColor colorRed( brightRed );
    static SetColor colorBlue( brightBlue );
    static SetColor colorGreen( brightGreen );
    static SetColor colorYellow( brightYellow );
    static SetColor colorViolet( brightViolet );
    static GroupEffect lines( new Line( 0, true ), new Line( 1, false ), new Line( 2, true ), new Line( 3, false ) );
    static GroupEffect colorLines( &colorRed, &lines,
                                   &colorBlue, &lines,
                                   &colorGreen, &lines,
                                   &colorViolet, &lines,
                                   &colorOff, &lines );

    static GroupEffect carusel( new Set(0), new Set(1), new Set(2), new Set(3) );

    static GroupEffect colorCarusel( &colorRed, &carusel,
                                     &colorBlue, &carusel,
                                     &colorGreen, &carusel,
                                     &colorOff, &carusel );

    static GroupEffect backOf( &colorRed, &allFromBack, &colorOff, &allFromBack,
                               &colorBlue, &allFromBack, &colorOff, &allFromBack,
                               &colorGreen, &allFromBack, &colorOff, &allFromBack );

    static GroupEffect colorTracer( &colorRed, &tracer,
                                    &colorBlue, &tracer,
                                    &colorGreen, &tracer,
                                    &colorYellow, &tracer,
                                    &colorViolet, &tracer,
                                    &colorOff, &tracer );

    static GroupEffect runners( new Runner( 0, true ), new Runner( 1, false ), new Runner( 2, true ), new Runner( 3, false ) );
    static GroupEffect colorRunners( &colorRed, &runners,
                                     &colorGreen, &runners,
                                     &colorYellow, &runners,
                                     &colorBlue, &runners );

    static GroupEffect mainEffect( &backOf, &colorLines, &colorCarusel, &colorTracer, &colorRunners );

    static bool init = true;

    if( !refresh ) {
        if( init ) {
            mainEffect.init();
            init = false;
        }
        if( !mainEffect.run() )
            init = true;
        refresh = true;
    }
    delay( 5 );
}

